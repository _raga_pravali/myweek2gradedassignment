package com.hcl.week2gradedassignment;

import java.util.ArrayList;
import java.util.Collections;

public class DataStructureA {
	public void sortingNames(ArrayList<Employee> employees) {
		ArrayList<String> employeeNames = new ArrayList<String>();
		for (Employee emp : employees) {
			employeeNames.add(emp.getName());
		}
		Collections.sort(employeeNames);
		System.out.println("Names of all the employees in sorted order are:");
		System.out.println(employeeNames);

	}
}
