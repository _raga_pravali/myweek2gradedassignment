
package com.hcl.week2gradedassignment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;

public class DataStructureB {
	public void cityNameCount(ArrayList<Employee> employees) {

		TreeMap<String, Integer> cityCount = new TreeMap<String, Integer>();
		for (Employee emp : employees) {
			String cityName = emp.getCity();
			if (cityCount.containsKey(cityName)) {
				cityCount.replace(cityName, ((cityCount.get(cityName)) + 1));
			} else {
				cityCount.put(cityName, 1);
			}
		}
		System.out.println("Count of employees from each city:");
		System.out.println(cityCount);
	}
}
		