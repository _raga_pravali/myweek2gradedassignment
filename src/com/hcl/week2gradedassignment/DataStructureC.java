package com.hcl.week2gradedassignment;

import java.util.ArrayList;
import java.util.TreeMap;

public class DataStructureC {
	public void monthlySalary(ArrayList<Employee> employees) {
			TreeMap<Integer, Float> employeeMonthlySalary_Id = new TreeMap<>();
			for (Employee emp : employees) {
				employeeMonthlySalary_Id.put(emp.getId(), (float) Math.floor((float) emp.getSalary() / 12));

			}
			System.out.println("Monthly salary of employees along with their id is: ");
			System.out.println(employeeMonthlySalary_Id);
		
	}

}
