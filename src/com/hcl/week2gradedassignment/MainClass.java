package com.hcl.week2gradedassignment;

import java.util.ArrayList;

public class MainClass {

	public static void main(String[] args) {
		try {
		Employee employee1 = new Employee();
		Employee employee2 = new Employee();	
		Employee employee3 = new Employee();
		Employee employee4 = new Employee();
		Employee employee5 = new Employee();
		
		// setting details of employee1
		employee1.setId(1);
		employee1.setName("Aman");
		employee1.setAge(20);
		employee1.setSalary(1100000);
		employee1.setDepartment("IT");
		employee1.setCity("Delhi");

		// setting details of employee2
		employee2.setId(2);
		employee2.setName("Bobby");
		employee2.setAge(22);
		employee2.setSalary(500000);
		employee2.setDepartment("HR");
		employee2.setCity("Bombay");

		// setting details of employee3
		employee3.setId(3);
		employee3.setName("Zoe");
		employee3.setAge(20);
		employee3.setSalary(750000);
		employee3.setDepartment("Admin");
		employee3.setCity("Delhi");

		// setting details of employee4
		employee4.setId(4);
		employee4.setName("Smitha");
		employee4.setAge(21);
		employee4.setSalary(100000);
		employee4.setDepartment("IT");
		employee4.setCity("Chennai");

		// setting details of employee5
		employee5.setId(5);
		employee5.setName("Smitha");
		employee5.setAge(24);
		employee5.setSalary(1200000);
		employee5.setDepartment("HR");
		employee5.setCity("Bangalore");

		ArrayList<Employee> employees = new ArrayList<Employee>();
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);
		employees.add(employee4);
		employees.add(employee5);
		for(Employee empDetails:employees) {
			if(empDetails.getId()<0||empDetails.getName()==null||empDetails.getName().isEmpty()||empDetails.getAge()<0||empDetails.getSalary()<0||empDetails.getDepartment()==null||empDetails.getDepartment().isEmpty()||empDetails.getCity()==null||empDetails.getCity().isEmpty()) {
				throw new IllegalArgumentException();
			}
		}

		System.out.println("List of Employees : ");
		System.out.println("Id     Name      Age      Salary(INR)    Department   Location");
		for (Employee emp : employees) {
			System.out.println(emp.getId() + "      " + emp.getName() + "     " + emp.getAge() + "      "
					+ emp.getSalary() + "          " + emp.getDepartment() + "          " + emp.getCity());
		}
		System.out.println();
		DataStructureA sortedNames = new DataStructureA();
		sortedNames.sortingNames(employees);
		DataStructureB employeeCount = new DataStructureB();
		employeeCount.cityNameCount(employees);
		DataStructureC salary = new DataStructureC();
		salary.monthlySalary(employees);


		}
		catch(IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}

}
}